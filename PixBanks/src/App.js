import React from 'react';
import {SafeAreaView} from 'react-native';
import Routes from './routes';

// import { Container } from './styles';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'yellow'}}>
      <Routes />
    </SafeAreaView>
  );
};

export default App;
