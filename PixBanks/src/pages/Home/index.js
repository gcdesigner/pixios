import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Button,
  Linking,
  TextInput,
  StyleSheet,
} from 'react-native';

// import { Container } from './styles';

const Home = () => {
  return (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1, paddingHorizontal: 20, paddingTop: 20}}>
        <View>
          <TextInput placeholder="Encontre seu banco" style={styles.input} />
        </View>

        <View style={{marginBottom: 20}}>
          <Button
            title="NUBANK"
            onPress={() => Linking.openURL('https://www.nubank.com.br/pagar/1')}
            color="#990080"
          />
        </View>

        <View style={{marginBottom: 20}}>
          <Button
            title="BANCO DO BRASIL"
            onPress={() => Linking.openURL('https://www.bb.com.br/')}
            color="#cdb602"
          />
        </View>

        <View style={{marginBottom: 20}}>
          <Button
            title="PIX BANCO 1"
            onPress={() => Linking.openURL('alpha://app')}
            color="#503215"
          />
        </View>

        <View style={{marginBottom: 20}}>
          <Button
            title="PIX BANCO 2"
            onPress={() => Linking.openURL('beta://app')}
            color="#588213"
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#fff',
    borderRadius: 50,
    paddingHorizontal: 20,
    marginBottom: 20,
  },
});

export default Home;
